<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '.::.', true, 'right' ); ?> <?php bloginfo('name'); ?></title>

  	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.ico" >
    <link href="<?php bloginfo('template_directory'); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/css/style.css" rel="stylesheet">
	<script src="<?php bloginfo('template_directory'); ?>/js/modernizr.custom.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
        	<?php wp_head(); ?>
<!-- Google Analytics -->
		<script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-17118502-15']);
            _gaq.push(['_trackPageview']);
            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>
        
        <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-50384702-1', 'lbmf.com.br');
		  ga('send', 'pageview');
		
		</script>
<!-- Google Analytics -->

  </head>
  <body itemscope itemtype='http://schema.org/WebPage'>

  	  <header class="hidden-xs hidden-sm menu-animado" id="menuFixed">
      	<div class="container">
            <div class="row">
            	<div class="col-md-2">
                	<a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/logo-lbmf.png" class="img-responsive"></a>
                </div>
                <div class="col-md-9 col-md-offset-1">
            <nav class="navbar navbar-principal">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
 					<div id="sb-search-fixed" class="sb-search">
						<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
							<input class="sb-search-input" placeholder="Digite o que você procura..." type="text" value="" name="s" id="search">
							<input class="sb-search-submit" type="submit" value="">
							<span class="sb-icon-search icon-magnifying-glass" style="background: #fbfbfb;"></span>
						</form>
					</div>

				<?php wp_nav_menu(array(
                'container'       => false,
                'items_wrap'      => '<ul id="%1$s" class="nav navbar-nav menu-principal">%3$s</ul>',
	            'theme_location'	=> 'principal',
	            'walker'          => new twitter_bootstrap_nav_walker
                ));
                ?>
    </div><!-- /.navbar-collapse -->

  </div><!-- /.container-fluid -->
</nav>
               </div>
            </div>
        </div>
      </header>

  	  <header class="header">
      	<div class="container">
        	<div class="menu-top">
				<p class="text-right" style="display:none;"><a href="#" class="btn-portfolio" target="_blank"> <i class="icon-box-add icon-portfolio"></i> Portfolio <span class="hidden-xs">(PDF)</span></a><a href="#" class="btn-usuario" data-toggle="modal" data-target="#myModal"> <i class="icon-users icon-area"></i> Área do Cliente</a><a href="<?php echo home_url(); ?>" class="btn-portugues">PT</a><a href="<?php echo home_url(); ?>/en" class="btn-ingles">EN</a></p>
            </div>
            <div class="row">
            	<div class="col-md-3 hidden-xs">
                	<a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/logo-lbmf.png" class="img-responsive"></a>
                </div>
                <div class="col-md-9">
            <nav class="navbar navbar-principal">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand visibile-xs hidden-sm hidden-md hidden-lg" href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/logo-lbmf.png" class="img-responsive"></a>
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
 					<div id="sb-search" class="sb-search">
						<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
							<input class="sb-search-input" placeholder="Digite o que você procura..." type="text" value="" name="s" id="search">
							<input class="sb-search-submit" type="submit" value="">
							<span class="sb-icon-search icon-magnifying-glass"></span>
						</form>
					</div>
				<?php wp_nav_menu(array(
                'container'       => false,
                'items_wrap'      => '<ul id="%1$s" class="nav navbar-nav menu-principal">%3$s</ul>',
	            'theme_location'	=> 'principal',
	            'walker'          => new twitter_bootstrap_nav_walker
                ));
                ?>

    </div><!-- /.navbar-collapse -->

  </div><!-- /.container-fluid -->
</nav>
               </div>
            </div>
        </div>
      </header>