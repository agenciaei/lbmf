<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
    	  <section class="hidden-xs hidden-sm feature bg-ibira" data-stellar-background-ratio="0.5"></section>
    	  <section class="visible-sm feature bg-ibira"></section>
    	  <section class="visible-xs feature-mobile bg-ibira-mobile"></section>
   	  <section class="sub-header">
      	<div class="container">
        	<div class="row">
            	<div class="col-md-2 col-sm-3 col-xs-12">
        	<div class="retangulo-page-header"></div>
            	</div>
                <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="page-header">
            	<h1 class="titulo-pagina">
                	<?php the_title(); ?><br>
                </h1>
                <div class="barra-page-header"></div>
                	<span class="data-noticia-home"><?php the_time('d/m/Y') ?></span>
            </div>
            </div>
            <div class="col-md-4 col-sm-3 col-xs-12">
            	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
            </div>
            </div>
        </div>
      </section>
      <section class="wrapper">
      	<div class="container">
        	<div class="row">
            	<div class="col-md-6 col-md-offset-2">
                	<div <?php _e('style="display:none;margin-bottom: 20px;"', 'lbmf'); ?>><?php echo do_shortcode('[google-translator]'); ?></div>
                	<?php the_content('Read the rest of this entry &raquo;'); ?>
                </div>
            	<div class="col-md-3 col-md-offset-1">
                    <img src="<?php bloginfo('template_directory'); ?>/img/lapis.jpg" alt="" class="img-responsive">
                </div>
            </div>
        </div>
      </section>
<?php endwhile; endif; ?>
  <?php get_footer(); ?>
