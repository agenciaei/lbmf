<?php

// Arquivos Externos
include_once('function/shortcodes.php');
include_once('function/profissionais.php');
include_once('function/slide.php');

/* Menus */

register_nav_menus( array(
		'principal' => __( 'Menu Principal', '' ),
    'profissionais' => __('Menu Profissionais',''),
		'atuacoes' => __('Menu Atuações','')
	) );

function fallbackmenu(){ ?>
							<ul><li> Go to Adminpanel > Appearance > Menus to create your menu. You should have WP 3.0+ version for custom menus to work.</li></ul>
<?php }	

/* FEATURED THUMBNAILS */

if ( function_exists( 'add_theme_support' ) ) { // Added in 2.9
	add_theme_support( 'post-thumbnails');
	add_image_size('internas-thumbnail', 640, 9999);
	add_image_size('pequeno', 360, 339, array( 'center', 'center' ));
	add_image_size('grande', 660, 300,  array( 'center', 'center' ));
	add_image_size('slide', 970, 500, array( 'center', 'center' ));


}

/* GET THUMBNAIL URL */

function get_image_url(){
	$image_id = get_post_thumbnail_id();
	$image_url = wp_get_attachment_image_src($image_id,'large');
	$image_url = $image_url[0];
	echo $image_url;
	}	

////////////////////////////////////////////////////////////////////////
// BREADCRUMBS
////////////////////////////////////////////////////////////////////////

function dimox_breadcrumbs() {
  if(!is_home()) {
    echo '<ol class="breadcrumb">';
    echo '<li><a href="'.get_option('home').'">Home</a></li>';
    if (is_single()) {
      echo '<li class="active">';
      the_category(', ');
      echo '</li>';
      if (is_single()) {
        echo '<li class="active">';
        the_title();
        echo '</li>';
      }
    } elseif (is_post_type_archive( $profissionais )){
      echo '<li class="active">';
      echo 'Profissionais';
      echo '</li>';
	} elseif (is_category()) {
      echo '<li class="active">';
      single_cat_title();
      echo '</li>';
    } elseif (is_page() && (!is_front_page())) {
      echo '<li class="active">';
      the_title();
      echo '</li>';
    } elseif (is_tag()) {
      echo '<li class="active">Tag: ';
      single_tag_title();
      echo '</li>';
    } elseif (is_day()) {
      echo'<li class="active">Archive for ';
      the_time('F jS, Y');
      echo'</li>';
    } elseif (is_month()) {
      echo'<li class="active">Archive for ';
      the_time('F, Y');
      echo'</li>';
    } elseif (is_year()) {
      echo'<li class="active">Archive for ';
      the_time('Y');
      echo'</li>';
    } elseif (is_author()) {
      echo'<li class="active">Author Archives';
      echo'</li>';
    } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
      echo '<li class="active">Blog Archives';
      echo'</li>';
    } elseif (is_search()) {
      echo'<li class="active">Search Results';
      echo'</li>';
    } else if ( get_post_type() != 'post' ) {
		$post_type = get_post_type_object(get_post_type());
		$slug = $post_type->rewrite;
		echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/" rel="nofollow">' . $post_type->labels->name . '</a>';
		if ($post->post_parent){
		$ancestors=get_post_ancestors($post->ID);
		$root=count($ancestors)-1;
		$parent = $ancestors[$root];
		echo '<a href="' .get_permalink($parent). '/" rel="nofollow">' . get_the_title($parent) . '</a>';
	    }
	}
    echo '</ol>';
  }
}

////////////////////////////////////////////////////////////////////////
// MENU MOBILE
////////////////////////////////////////////////////////////////////////

class Walker_Nav_Menu_Dropdown extends Walker_Nav_Menu{
    function start_lvl(&$output, $depth){
      $indent = str_repeat("\t", $depth); // don't output children opening tag (`<ul>`)
    }

    function end_lvl(&$output, $depth){
      $indent = str_repeat("\t", $depth); // don't output children closing tag
    }

    function start_el(&$output, $item, $depth, $args){
      // add spacing to the title based on the depth
      $item->title = str_repeat("&nbsp;", $depth * 4).$item->title;


        $output .= $indent . $value . $class_names;  
        //$output .= $indent . ' id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';  
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';  
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';  
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';  
        $attributes .= ! empty( $item->url )        ? ' value="'   . esc_attr( $item->url        ) .'"' : '';  
        
        $item_output .= '<option'. $attributes .'>';  
        $item_output .= $args->link_before .apply_filters( 'the_title', $item->title, $item->ID );  
        $item_output .= '</option>';  
        
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );  



      // no point redefining this method too, we just replace the li tag...
      $output = str_replace('<li', '<option', $output);
    }

    function end_el(&$output, $item, $depth){
      $output .= "</option>\n"; // replace closing </li> with the option tag
    }
}

// Add Twitter Bootstrap's standard 'active' class name to the active nav link item
add_filter('nav_menu_css_class', 'add_active_class', 10, 2 );
function add_active_class($classes, $item) {
	if( $item->menu_item_parent == 0 && in_array('current-menu-item', $classes) ) {
    $classes[] = "active";
	}
  
  return $classes;
}

class twitter_bootstrap_nav_walker extends Walker_Nav_Menu {

function start_lvl( &$output, $depth ) {
$indent = str_repeat( "\t", $depth );
$output	.= "\n$indent<ul class=\"dropdown-menu\">\n";
}

function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
global $wp_query;
$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

if (strcasecmp($item->title, 'divider')) {
$class_names = $value = '';
$classes = empty( $item->classes ) ? array() : (array) $item->classes;
$classes[] = ($item->current) ? 'active' : '';
$classes[] = 'menu-item-' . $item->ID;
$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );

if ($args->has_children && $depth > 0) {
$class_names .= ' dropdown-submenu';
} else if($args->has_children && $depth === 0) {
$class_names .= ' dropdown';
}

$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

$output .= $indent . '<li' . $id . $value . $class_names .'>';

$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';
$attributes .= ($args->has_children) ? ' data-toggle="dropdown" class="dropdown-toggle"' : '';

$item_output = $args->before;
$item_output .= '<a'. $attributes .'>';
$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
$item_output .= ($args->has_children && $depth == 0) ? ' <b class="caret"></b></a>' : '</a>';
$item_output .= $args->after;

$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
} else {
$output .= $indent . '<li class="divider">';
}
}

function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

if ( !$element ) {
return;
}

$id_field = $this->db_fields['id'];

if ( is_array( $args[0] ) )
$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
else if ( is_object( $args[0] ) )
$args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
$cb_args = array_merge( array(&$output, $element, $depth), $args);
call_user_func_array(array(&$this, 'start_el'), $cb_args);

$id = $element->$id_field;

if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {

foreach( $children_elements[ $id ] as $child ){

if ( !isset($newlevel) ) {
$newlevel = true;

$cb_args = array_merge( array(&$output, $depth), $args);
call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
}
$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
}
unset( $children_elements[ $id ] );
}

if ( isset($newlevel) && $newlevel ){

$cb_args = array_merge( array(&$output, $depth), $args);
call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
}
$cb_args = array_merge( array(&$output, $element, $depth), $args);
call_user_func_array(array(&$this, 'end_el'), $cb_args);
}
}

function add_responsive_class($content){

        $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
        $document = new DOMDocument();
        libxml_use_internal_errors(true);
        $document->loadHTML(utf8_decode($content));

        $imgs = $document->getElementsByTagName('img');
        foreach ($imgs as $img) {           
		   $existing_class = $img->getAttribute('class');
		   $img->setAttribute('class', "img-responsive $existing_class");
        }

        $html = $document->saveHTML();
        return $html;   
}

add_filter('the_content', 'add_responsive_class');


//attach our function to the wp_pagenavi filter
add_filter( 'wp_pagenavi', 'ik_pagination', 10, 2 );
  
//customize the PageNavi HTML before it is output
function ik_pagination($html) {
    $out = '';
  
    //wrap a's and span's in li's
    $out = str_replace("<div","",$html);
    $out = str_replace("class='wp-pagenavi'>","",$out);
    $out = str_replace("<a","<li><a",$out);
    $out = str_replace("</a>","</a></li>",$out);
    $out = str_replace("<span","<li><span",$out);  
    $out = str_replace("</span>","</span></li>",$out);
    $out = str_replace("</div>","",$out);
  
    return '<ul class="pagination">'.$out.'</ul>';
}

add_action( 'pre_get_posts', 'my_change_sort_order'); 
    function my_change_sort_order($query){
        if(is_post_type_archive( $profissionais )):
           $query->set( 'order', 'ASC' );
        endif;    
    };

?>