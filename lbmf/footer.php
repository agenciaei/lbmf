      <footer class="footer">
		<div class="container" style="position:relative;">
        	<div class="retangulo-footer"></div>
            
		</div>
      	<section class="hidden-xs bg-footer" data-stellar-background-ratio="0.5"></section>
      	<section class="visible-xs bg-footer-mobile"></section>
      	<section class="conteudo-footer">
 			<div class="container">
            	<div class="row">
                	<div class="col-md-3 col-sm-3">
                    	<img src="<?php bloginfo('template_directory'); ?>/img/logo-lbmf-rodape.png" class="img-responsive logo-footer" alt="LBMF">
                        <p class="social-footer"><a href="https://www.facebook.com/pages/LBMF-Advocacia-de-Neg%C3%B3cios/131743173674710" target="_blank"><i class="icon-rodape icon-facebook"></i></a><a href="http://www.linkedin.com/company/2309451?trk=prof-0-ovw-curr_pos" target="_blank"><i class="icon-rodape icon-linkedin"></i></a></p>
                    </div>
                	<div class="col-md-offset-1 col-md-4 col-sm-4">
                    	<p><?php _e('LBMF | BARBOSA & FERRAZ IVAMOTO ADVOGADOS surgiu da união de seus sócios fundadores, Luís Alexandre Barbosa e Mônica Ferraz Ivamoto, especializados na área tributária, societária e aduaneira, provenientes de renomados escritórios de advocacia e com relevante atuação nacional e internacional.', 'lbmf'); ?></p>
                    </div>
                	<div class="col-md-3 col-md-offset-1 col-sm-5">
                    	<div class="box-contato-footer">
                        	<i class="icones-contato icon-office"></i>
                            <p class="texto-contato">Av. Pres. Juscelino Kubitschek, nº 360<br>
							16º andar | Conj. 162 | 04543-000 |<br>
                            Itaim Bibi | São Paulo</p>
                            <div class="clearfix"></div>
                            <i class="icones-contato glyphicon glyphicon-map-marker"></i>
                            <p class="texto-contato"><a href="https://www.google.com.br/maps/place/LBMF+%7C+Barbosa+e+Ferraz+Ivamoto+Sociedade+de+Advogados/@-23.586106,-46.6756927,17z/data=!3m1!4b1!4m5!3m4!1s0x94ce575939c12683:0x3ee36880d99c3251!8m2!3d-23.586106!4d-46.673504" target="_blank"><?php _e('Veja como chegar!', 'lbmf'); ?></a><br><a href="https://waze.to/lr/h6gyccfm2u" target="_blank"><?php _e('Traçar Rota com o Waze.', 'lbmf'); ?></a></p>
                            <div class="clearfix"></div>
                    	</div>
                		<div class="box-contato-footer">
                        	<i class="icones-contato icon-old-phone"></i>
                            <p class="texto-contato">+55 (11) 4118.1000</p>
                            <div class="clearfix"></div>
                    	</div>
                		<div class="box-contato-footer">
                        	<i class="icones-contato icon-mail"></i>
                            <p class="texto-contato"><a href="mailto:lbmf@lbmf.com.br">lbmf@lbmf.com.br</a></p>
                            <div class="clearfix"></div>
                    	</div>
                    </div>
                </div>
            </div>       
        </section>
		<section class="copyright">
        	<div class="container">
            	<p class="pull-left">© 2016 Todos os Direitos Reservados - LBMF | BARBOSA & FERRAZ IVAMOTO ADVOGADOS</p>
                <a href="http://www.equipedeideias.com.br" rel="nofollow" target="_blank" class="pull-right"><img src="<?php bloginfo('template_directory'); ?>/img/logo-ei.png"></a>
            </div>
        </section>        
      </footer>
    <?php wp_footer(); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/classie.js"></script>
	<script src="<?php bloginfo('template_directory'); ?>/js/uisearch.js"></script>
	<script src="<?php bloginfo('template_directory'); ?>/js/jquery.stellar.min.js"></script>	
	<script src="<?php bloginfo('template_directory'); ?>/js/custom.js"></script>	
<!-- Modal Área do Cliente-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php _e('Área do Clientes', 'lbmf'); ?></h4>
      </div>
      <div class="modal-body">
        <form>
  <div class="form-group">
    <label for="exampleInputEmail1"><?php _e('Nome do Usuário', 'lbmf'); ?></label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Email">
  </div>
  <button type="submit" class="btn-enviar"><?php _e('Entrar', 'lbmf'); ?></button>
</form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
  </body>
</html>