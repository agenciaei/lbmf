<div id="search" class="box-busca-top">	
	<form role="search" method="get" id="searchform" class="form-inline" action="<?php echo home_url( '/' ); ?>">
	<div class="form-group">
        <input type="text" class="form-control input-busca" name="s" value="Digite o que procura" onblur="if (this.value == '') {this.value = 'Digite o que procura';}" onfocus="if (this.value == 'Digite o que procura') {this.value = '';}"><button type="submit" class="btn-busca"><span class="glyphicon glyphicon-search"></span></button>
        </div>
</form>
</div>
