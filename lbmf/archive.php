<?php get_header(); ?>
    	  <section class="hidden-xs hidden-sm feature bg-ibira" data-stellar-background-ratio="0.5"></section>
       	  <section class="visible-sm feature bg-ibira"></section>
       	  <section class="visible-xs feature-mobile bg-ibira-mobile"></section>
   	  <section class="sub-header">
      	<div class="container">
        	<div class="row">
            	<div class="col-md-2 col-sm-3 col-xs-12">
        	<div class="retangulo-page-header"></div>
            	</div>
                <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="page-header">
            <?php $categoria = get_the_category();
			  $nomeCategoria = single_cat_title("", false);
		?>
        <?php if ($nomeCategoria == "Notícias"){ ?>
            	<h1 class="titulo-pagina">
                	Notícias<br>
                </h1>
                    <small>Veja mais informações e atualidades na área de negócios.</small>
                    <?php }?>
            	<?php if ($nomeCategoria == "Informativos"){ ?>
            	<h1 class="titulo-pagina">
                	Informativos<br>
                </h1>
                    <small>Clique em cada link para acesso e download dos informativos publicados pelo LBMF.</small>
                    <?php }?>
                <div class="barra-page-header"></div>
            </div>
            </div>
            <div class="col-md-4 col-sm-3 col-xs-12">
            	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
            </div>
            </div>
        </div>
      </section>
      <section class="wrapper">
      	<div class="container">
            <div class="row">
            	<div class="col-md-2 hidden-sm hidden-xs">

                </div>
            	<div class="col-md-6  col-sm-8">
                   <?php if (have_posts()) : ?>
				   <?php while (have_posts()) : the_post(); ?>
				   
                    <span class="data-noticia-home"><?php the_time('d/m/Y') ?></span>
                    <h3 class="titulo-noticia-home"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                    <div class="barra-noticia-home"></div>

                    <?php endwhile; endif; ?>
					<div class="text-right"><?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?></div>
                </div>
            	<div class="col-md-3 col-md-offset-1 col-sm-4">
                    <img src="<?php bloginfo('template_directory'); ?>/img/lapis.jpg" alt="" class="img-responsive">
                </div>
            </div>
        </div>
      </section>
  <?php get_footer(); ?>