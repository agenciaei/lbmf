<?php get_header(); ?>
    	  <section class="hidden-xs hidden-sm feature bg-luminaria" data-stellar-background-ratio="0.5"></section>
    	  <section class="visible-sm feature bg-luminaria"></section>
    	  <section class="visible-xs feature-mobile bg-luminaria-mobile"></section>
   	  <section class="sub-header">
      	<div class="container">
        	<div class="row">
            	<div class="col-md-2">
        	<div class="retangulo-page-header"></div>
            	</div>
                <div class="col-md-6">
            <div class="page-header">
            	<h1 class="titulo-pagina">
                	<?php $mySearch =& new WP_Query("s=$s & showposts=-1 & order=ASC & orderby=title");	$num = $mySearch->post_count;	echo $num.' resultados para sua busca por: '; the_search_query();?>
                </h1>
                <div class="barra-page-header"></div>
            </div>
            </div>
            <div class="col-md-4">
            	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
            </div>
            </div>
        </div>
      </section>
      <section class="wrapper">
      	<div class="container">
            <div class="row">
            	<div class="col-md-2">

                </div>
            	<div class="col-md-6">
                   <?php if (have_posts()) : ?>
				   <?php while (have_posts()) : the_post(); ?>
				   
                    <span class="data-noticia-home"><?php the_time('d/m/Y') ?></span>
                    <h3 class="titulo-noticia-home"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                    <div class="barra-noticia-home"></div>

                    <?php endwhile; endif; ?>
					<div class="text-right"><?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?></div>
                </div>
            	<div class="col-md-3 col-md-offset-1">
                    <img src="<?php bloginfo('template_directory'); ?>/img/img-noticias-home-marcacao.jpg" alt="" class="img-responsive">
                </div>
            </div>
        </div>
      </section>
  <?php get_footer(); ?>
  
<?php get_header();?>
    	<div class="col-md-9">
        	<div class="box-titulo-categoria wiki">
                <h2 class="titulo-categoria">( <span class="strong">S</span> XX <span class="strong">Pedia</span> )</h2>
            </div>
<?php if (have_posts()) : ?>

			<h1 class="titulo-chamadas text-center"><?php $mySearch =& new WP_Query("s=$s & showposts=-1 & order=ASC & orderby=title");	$num = $mySearch->post_count;	echo $num.' resultados para sua busca por: '; the_search_query();?></h1>
<?php while (have_posts()) : the_post(); ?>
        		<h2 class="titulo-wiki"><?php the_title(); ?></h2>
                <p><?php the_content(); ?></p>
			<hr>

<?php endwhile; ?>
			<div class="text-center"><?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?></div>

<?php else : ?>
            <h2 class="titulo-wiki">Sua pesquisa por <?php the_search_query();?> não encontrou nenhum resultado.</h2>
<p>Sugestões:</p>
<ul>
   <li>  Certifique-se de que todas as palavras estão escritas corretamente..</li>
   <li>  Tente palavras-chave diferentes..</li>
   <li>  Tente palavras-chave mais gerais.</li>
</ul>

<?php endif; ?>

        </div>
    </div>
</section>
  <?php get_footer(); ?>