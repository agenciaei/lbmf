		new UISearch( document.getElementById( 'sb-search' ) );
		new UISearch( document.getElementById( 'sb-search-fixed' ) );

		$(function(){
			$.stellar({
				horizontalScrolling: false,
				verticalOffset: 40
			});
		});

jQuery(document).ready(function($) {
  $('ul.nav li.dropdown, ul.nav li.dropdown-submenu').hover(function() {
                                $(this).find(' > .dropdown-menu').stop(true, true).fadeIn();
                }, function() {
                               $(this).find(' > .dropdown-menu').stop(true, true).fadeOut();
                });

             
});

		$(document).ready(function() {
		  var menuFixed = $('#menuFixed');
				 $(window).scroll(function () {
					  if ($(this).scrollTop() > 150) {
						 menuFixed.addClass("menu-animacao");
					  } else {
						  menuFixed.removeClass("menu-animacao");
					  }
				});

 var atuacaoFixed = $('#menu-atuacoes');
 var atuacaoFixedEN = $('#menu-atuacoes-ingles');
				 $(window).scroll(function () {
					  if ($(this).scrollTop() > 400) {
						 atuacaoFixed.addClass("menu-atuacoesFixed");
						 atuacaoFixedEN.addClass("menu-atuacoesFixed");
					  } else {
					  	atuacaoFixed.removeClass("menu-atuacoesFixed")
					  	atuacaoFixedEN.removeClass("menu-atuacoesFixed")
					  }
				});

$("ul.menu-atuacoes > li > a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top - 120
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
		});	
