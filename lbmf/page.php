<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
    	  <section class="hidden-xs hidden-sm feature bg-<?php $imagem = get_post_meta( $post->ID, 'imagem', true ); echo $imagem; ?>" data-stellar-background-ratio="0.5"></section>
    	  <section class="hidden-xs visible-sm feature bg-<?php $imagem = get_post_meta( $post->ID, 'imagem', true ); echo $imagem; ?>"></section>
    	  <section class="visible-xs feature-mobile bg-<?php $imagem = get_post_meta( $post->ID, 'imagem', true ); echo $imagem; ?>-mobile"></section>
   	  <section class="sub-header">
      	<div class="container">
        	<div class="row">
            	<div class="col-md-2 col-sm-3 col-xs-12">
        	<div class="retangulo-page-header"></div>
            	</div>
                <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="page-header">
            	<h1 class="titulo-pagina">
                	<?php the_title(); ?><br>
                </h1>
                    <small><?php $texto = get_post_meta( $post->ID, 'texto', true ); echo $texto; ?></small>
                <div class="barra-page-header"></div>
            </div>
            </div>
            <div class="col-md-4 col-sm-3 col-xs-12">
            	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
            </div>
            </div>
        </div>
      </section>
      <section class="wrapper">
      	<div class="container">
        	<div class="row">
            	<div class="col-md-6 col-md-offset-2 col-sm-8">
                	<?php the_content('Read the rest of this entry &raquo;'); ?>
                </div>
            	<div class="col-md-3 col-md-offset-1 col-sm-4">
                	<?php $imagem =  wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' ); ?>
                    <img src="<?php echo $imagem[0]; ?>" alt="" class="img-responsive">
                </div>
            </div>
        </div>
      </section>
<?php endwhile; endif; ?>
  <?php get_footer(); ?>
  
