<?php get_header(); ?>
    	  <section class="hidden-xs hidden-sm feature bg-rio" data-stellar-background-ratio="0.5"></section>
       	  <section class="visible-sm feature bg-rio"></section>
       	  <section class="visible-xs feature-mobile bg-rio-mobile"></section>
   	  <section class="sub-header">
      	<div class="container">
        	<div class="row">
            	<div class="col-md-2 col-sm-3 col-xs-12">
        	<div class="retangulo-page-header"></div>
            	</div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="visible-xs" style="margin-top: 120px;"></div>
            <div class="page-header">
            	<h1 class="titulo-pagina">
                	<?php _e('Profissionais', 'lbmf'); ?><br>
                </h1>
                    <small><?php _e('Conheça a equipe de profissionais que compõe o escritório', 'lbmf'); ?></small>
                <div class="barra-page-header"></div>
            </div>
            </div>
            <div class="col-md-4 col-sm-3 col-xs-12">
            	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
            </div>
            </div>
        </div>
      </section>
      <section class="wrapper">
      	<div class="container">
            <div class="row">
            	<div class="col-md-2">
			<?php /*wp_nav_menu(array(
            'container'       => false,
            'items_wrap'      => '<ul id="%1$s" class="letras">%3$s</ul>',
            'theme_location'	=> 'profissionais',
            'walker'          => new twitter_bootstrap_nav_walker
            ));*/
            ?>
                </div>
            	<div class="col-md-6 col-sm-8">
                   <?php if (have_posts()) : ?>
				   <?php while (have_posts()) : the_post(); 
				   
$cargo = get_post_meta( $post->ID, '_profissionais_cargo', true );
$area = get_post_meta( $post->ID, '_profissionais_area', true );
$email = get_post_meta( $post->ID, '_profissionais_email', true );
$telefone = get_post_meta( $post->ID, '_profissionais_telefone', true );
$vcard = get_post_meta( $post->ID, '_profissionais_vcard', true );
$linguas1 = get_post_meta( $post->ID, '_profissionais_linguas1', true );
$linguas2 = get_post_meta( $post->ID, '_profissionais_linguas2', true );
$linguas3 = get_post_meta( $post->ID, '_profissionais_linguas3', true );
$linguas4 = get_post_meta( $post->ID, '_profissionais_linguas4', true );
$background1 = get_post_meta( $post->ID, '_profissionais_background1', true );
$background2 = get_post_meta( $post->ID, '_profissionais_background2', true );
$background3 = get_post_meta( $post->ID, '_profissionais_background3', true );
$background4 = get_post_meta( $post->ID, '_profissionais_background4', true );

				   
				   ?>

    				<div class="row box-profissionais">
                    	<!--<div class="col-md-4 col-sm-4">
                        	<?php //$imagem =  wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' ); ?>
                        	<a href="<?php the_permalink(); ?>"><img src="<?php //echo $imagem[0]; ?>" alt="" class="img-responsive" class="<?php //the_title(); ?>"></a>
                        </div>-->
                    	<div class="col-md-8 col-sm-8">
                        <h2 class="titulo-profissional"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <div class="barra-titulo-profissional"></div>
                        <p><?php echo $cargo; ?><br>
                        <?php echo $area; ?></p>
                        <div class="barra-area-profissional"></div>
                        <p class="info-profissional"><a href="mailto:<?php echo $email; ?>" class="email-prof"><?php echo $email; ?></a><br>
                        +55 11 4118-1000 / <?php echo $telefone; ?><br>
                        <a href="http://www.lbmf.com.br/vCards/<?php echo $vcard; ?>" class="btn-vCard">Download do vCard</a></p>
                        </div>
                    </div>
                    <?php endwhile; endif; ?>
					<div class="text-right"><?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?></div>
                </div>
            	<div class="col-md-3 col-md-offset-1 col-sm-4">
                <h2 class="titulo-home text-right"><?php _e('DESENVOLVIMENTO<br>PROFISSIONAL', 'lbmf'); ?></h2>
                    <div class="barra-titulo-oportunidade"></div>
                    <p class="text-right texto-oportunidade"><?php _e('Entenda como os profissionais do LBMF constantemente crescem e ampliam sua experiência.', 'lbmf'); ?></p>
                    <p class="text-right"><a href="<?php echo home_url(); ?>/<?php _e('desenvolvimento-profissional', 'lbmf'); ?>" class="btn-lateral"><?php _e('CONHEÇA', 'lbmf'); ?></a></p>
                <h2 class="titulo-home text-right"><?php _e('OPORTUNIDADES<br>DE CARREIRA', 'lbmf'); ?></h2>
                    <div class="barra-titulo-oportunidade"></div>
                    <p class="text-right texto-oportunidade"><?php _e('Saiba como ser parte da equipe LBMF.', 'lbmf'); ?></p>
                    <p class="text-right"><a href="<?php echo home_url(); ?>/<?php _e('oportunidades-de-carreira', 'lbmf'); ?>" class="btn-lateral"><?php _e('FAÇA PARTE', 'lbmf'); ?></a></p>
                    <img src="<?php bloginfo('template_directory'); ?>/img/cadeira.jpg" alt="" class="img-responsive">
                </div>
            </div>
        </div>
      </section>
  <?php get_footer(); ?>