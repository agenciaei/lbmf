	<?php get_header();?>
    
      	  <section class="feature-home hidden-xs">
      	        <ul class="cb-slideshow">
                <?php 
	  
	  $newsArgs = array( 'post_type' => 'slide', 'order' => 'ASC');                   
                        
      $newsLoop = new WP_Query( $newsArgs );                  
                        
      while ( $newsLoop->have_posts() ) : $newsLoop->the_post();
	  
	  $html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
	  $texto = get_post_meta( $post->ID, '_slide_subtitulo', true );

	?>
            <li><span style="background-image:url(<?php echo $html[0]; ?>);"></span><div class="box-slider"><h3><?php the_title(); ?></h3><div class="barra-titulo-slider"></div><h4><?php echo $texto; ?></h4></div></li>
            
            <?php endwhile; ?>
        </ul>
      </section>
   	  <section class="sub-header hidden-xs">
      	<div class="container">
        	<div class="row">
            	<div class="col-md-2">
        	<div class="retangulo-header"></div>
            	</div>
            </div>
        </div>
      </section>
      <section class="wrapper">
      	<div class="container">
        	<div class="row">
            	<div class="col-md-3 hidden-sm">
                	<h2 class="titulo-home text-right"><?php _e('NOTÍCIAS', 'lbmf'); ?></h2>
                    <div class="barra-titulo-noticia"></div>
                    <img src="<?php bloginfo('template_directory'); ?>/img/fachada.jpg" alt="" class="img-responsive img-noticia-home">
                </div>
            	<div class="col-md-4 col-md-offset-1 col-sm-12">
                	<div class="margin-noticia-home hidden-sm"></div>
                    <h2 class="titulo-home text-left visible-sm"><?php _e('NOTÍCIAS', 'lbmf'); ?></h2>
                    <div class="barra-titulo-informativo visible-sm"></div>
                    <div <?php _e('style="display:none;margin-bottom: 20px;"', 'lbmf'); ?>><?php echo do_shortcode('[google-translator]'); ?></div>
					<?php
                    
					$cont = 0;
					query_posts( array( 'cat' => 2, 'post_type' => 'post', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page'=> 5) ); 
                			while (have_posts()) : the_post();
							
							$cont++;
					?>
                    <span class="data-noticia-home"><?php the_time('d/m/Y') ?></span>
                    <h3 class="titulo-noticia-home"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                    <?php if($cont < 5){ ?>
                    <div class="barra-noticia-home"></div>
                    <?php } ?>
                    <?php endwhile; ?>
                    <a href="<?php echo home_url(); ?>/noticias" class="btn-noticias-home pull-right"><?php _e('VEJA TODAS AS NOTÍCIAS', 'lbmf'); ?></a>
                    <div class="clearfix"></div>
                </div>
            	<div class="col-md-3 col-md-offset-1 col-sm-12">
                	<h2 class="titulo-home hidden-xs"><?php _e('INFORMATIVOS', 'lbmf'); ?></h2>
                	<h2 class="titulo-home-mobile visible-xs"><?php _e('INFORMATIVOS', 'lbmf'); ?></h2>
                    <div class="barra-titulo-informativo"></div>
                    <div class="margin-informativo-home"></div>
                    <?php
                    
					$cont = 0;
					query_posts( array( 'cat' => 41, 'post_type' => 'post', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page'=> 4) ); 
                			while (have_posts()) : the_post();
							
							$cont++;
					?>                    
                    <span class="data-informativo-home"><?php the_time('d/m/Y') ?></span>
                    <h3 class="titulo-informativo-home"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                    <p class="text-right"><a href="<?php the_permalink(); ?>" class="btn-download">DOWNLOAD</a></p>
                    <?php endwhile; ?>
                    <a href="<?php echo home_url(); ?>/informativos" class="btn-noticias-home pull-right"><?php _e('VEJA TODOS OS INFORMATIVOS', 'lbmf'); ?></a>
                </div>
            </div>
        </div>
      </section>
<?php get_footer(); ?>