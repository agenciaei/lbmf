        	<div class="col-md-3">
					<div class="barra-lateral hidden-xs hidden-sm">
                    	<div class="logo">
                    		<a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo-acustica-sao-luiz.png" alt="Acústica São Luiz" class="img-responsive"></a>
                    	</div>
                        <!-- Inicio Menu -->
                            <nav class="navbar navbar-default" role="navigation">
                              <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-principal">
                                  <span class="sr-only">Navegue</span>
                                  <span class="icon-bar"></span>
                                  <span class="icon-bar"></span>
                                  <span class="icon-bar"></span>
                                </button>
                              </div>
                          <div class="collapse navbar-collapse" id="menu-principal">
                                  					<?php wp_nav_menu(array(
                'container'       => false,
                'items_wrap'      => '<ul id="%1$s" class="%2$s nav nav-pills nav-stacked">%3$s</ul>',
                'walker'          => new twitter_bootstrap_nav_walker
                ));
                ?>

                          </div>
                        </nav>
                        <!-- Fim Menu -->
                    	<div class="midias">
                        	<a href="https://www.facebook.com/pages/Ac%C3%BAstica-S%C3%A3o-Luiz/361958670547556" class="facebook social-media" target="_blank">Facebook</a>
            				<a href="http://www.youtube.com/user/acusticasaoluiz" class="youtube social-media" target="_blank">Youtube</a>
            				<!--<a href="#" class="googleplus social-media" target="_blank">Google +</a>-->
							<div class="clearfix"></div>
                    	</div>
					</div>				
            </div>