<?php

add_action( 'init', 'slide_post_type' );
function slide_post_type() {
	register_post_type( 'slide',
		array(
			'labels' => array(
				'name' 				=> 'Slides',
				'singular_name' 	=> 'Slides',
				'menu_name'         => 'Slides',
				'all_items'         => 'Todos os Slides',
				'view_item'         => 'Ver Slide',
				'add_new_item'      => 'Adicionar novo Slide',
				'add_new'           => 'Adicionar Slide',
				'edit_item'         => 'Alterar Slide',
				'update_item'       => 'Atualizar Slide',
				'search_items'      => 'Pesquisar Slide',
				'not_found'         => 'Nenhum Slide Encontrado',
				'not_found_in_trash'=> 'Nenhum Slide Encontrado na Lixeira',
			),
		'hierarchical' 		  => true,
		'has_archive' 		  => true,
		'public' 			  => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'menu_icon' 		  => 'dashicons-images-alt',
    	'menu_position' => 5,
		'supports'            => array( 'title', 'thumbnail', 'page-attributes'),
		'rewrite'             => array( 'slug' => 'slides'),
    	)
	);
	
	flush_rewrite_rules();
}
 
function ep_slideposts_metaboxes() {
	add_meta_box( 'ept_slide_subtitulo', 'Subtitulo', 'ept_slide_subtitulo', 'slide', 'normal', 'default', array('id'=>'_subtitulo') );
}
add_action( 'admin_init', 'ep_slideposts_metaboxes' );
 
function ept_slide_subtitulo() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_slideposts_nonce' );
    // The metabox HTML
    $slide_subtitulo = get_post_meta( $post->ID, '_slide_subtitulo', true );
    echo '<input type="text" name="_slide_subtitulo" value="' . $slide_subtitulo  . '"  style="width:99%"/>';
}

// Save the Metabox Data
function ep_slideposts_save_meta( $post_id, $post ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( !isset( $_POST['ep_slideposts_nonce'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['ep_slideposts_nonce'], plugin_basename( __FILE__ ) ) )
        return;
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ) )
        return;
 
    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though
 	$slide_meta_save['_slide_subtitulo'] = $_POST['_slide_subtitulo'];
 	
    // Add values of $events_meta as custom fields
    foreach ( $slide_meta_save as $key => $value ) { // Cycle through the $events_meta array!
        if ( $post->post_type == 'revision' ) return; // Don't store custom data twice
        $value = implode( ',', (array)$value ); // If $value is an array, make it a CSV (unlikely)
        if ( get_post_meta( $post->ID, $key, false ) ) { // If the custom field already has a value
            update_post_meta( $post->ID, $key, $value );
        } else { // If the custom field doesn't have a value
            add_post_meta( $post->ID, $key, $value );
        }
        if ( !$value ) delete_post_meta( $post->ID, $key ); // Delete if blank
    }
}
add_action( 'save_post', 'ep_slideposts_save_meta', 1, 2 );

?>