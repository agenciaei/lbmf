<?php

/**
 * Adicionamos uma acção no inicio do carregamento do WordPress
 * através da função add_action( 'init' )
 */

add_action( 'init', 'profissionais' );


/**
 * Esta é a função que é chamada pelo add_action()
 */
function profissionais() {

    /**
     * Labels customizados para o tipo de post
     * 
     */
    $labels = array(
	    'name' => _x('Profissionais', 'post type general name'),
	    'singular_name' => _x('Item profissionais', 'post type singular name'),
	    'add_new' => _x('Adicionar Novo', 'profissionais'),
	    'add_new_item' => __('Adicionar Novo'),
	    'edit_item' => __('Editar Item'),
	    'new_item' => __('Novo Item'),
	    'all_items' => __('Todos os Itens'),
	    'view_item' => __('Ver item'),
	    'search_items' => __('Pesquisar Item'),
	    'not_found' =>  __('Nenhum item encontrado'),
	    'not_found_in_trash' => __('Nenhum item encontrado na lixeira'),
	    'parent_item_colon' => '',
	    'menu_name' => 'Profissionais'
    );
    
    /**
     * Registamos o tipo de post através desta função
     * passando-lhe os labels e parâmetros de controlo.
     */
    register_post_type( 'profissionais', array(
	    'labels' => $labels,
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true,
	    'show_in_menu' => true,
	    'has_archive' => 'archive-profissionais',
	    'rewrite' => array(
		 'slug' => 'archive-profissionais',
		 'with_front' => false
	    ),
	    'capability_type' => 'post',
	    'menu_icon' => 'dashicons-admin-users',
		'has_archive' => true,
	    'hierarchical' => false,
	    'menu_position' => 5,
	    'supports' => array('title','editor','thumbnail', 'page-attributes')
	    )
    );
    
}

 /**
     * Registamos a categoria de filmes para o tipo de post film
     
    register_taxonomy( 'profissionais_category', array( 'profissionais' ), array(
        'hierarchical' => true,
        'label' => __( 'Alfabeto' ),
        'labels' => array( // Labels customizadas
	    'name' => _x( 'Alfabeto', 'taxonomy general name' ),
	    'singular_name' => _x( 'Alfabeto', 'taxonomy singular name' ),
	    'search_items' =>  __( 'Pesquisar Alfabeto' ),
	    'all_items' => __( 'Todas Alfabeto' ),
	    'parent_item' => __( 'Categoria Alfabeto' ),
	    'parent_item_colon' => __( 'Categoria Alfabeto:' ),
	    'edit_item' => __( 'Editar Alfabeto' ),
	    'update_item' => __( 'Atualizar Alfabeto' ),
	    'add_new_item' => __( 'Adicionar Alfabeto' ),
	    'new_item_name' => __( 'Nome Nova Alfabeto' ),
	    'menu_name' => __( 'Alfabeto' ),
	),
        'show_ui' => true,
        'show_in_tag_cloud' => true,
        'query_var' => true,
        'rewrite' => array(
            'slug' => 'letra',
			'with_front' => true
        ),
        )
    );
    */
    /** 
     * Esta função associa tipos de categorias com tipos de posts.
     * Aqui associamos as tags ao tipo de post film.
     
    register_taxonomy_for_object_type( 'tags', 'profissionais' );
	*/
	
function ep_profissionaisposts_metaboxes() {
	add_meta_box( 'ept_profissionais_infos', 'Informações', 'ept_profissionais_infos', 'profissionais', 'normal', 'default', array('id'=>'_infos') );
	add_meta_box( 'ept_profissionais_contatos', 'Contatos', 'ept_profissionais_contatos', 'profissionais', 'normal', 'default', array('id'=>'_contatos') );
	add_meta_box( 'ept_profissionais_linguas', 'Línguas', 'ept_profissionais_linguas', 'profissionais', 'normal', 'default', array('id'=>'_linguas') );
	add_meta_box( 'ept_profissionais_background', 'Background', 'ept_profissionais_background', 'profissionais', 'normal', 'default', array('id'=>'_background') );
}
add_action( 'admin_init', 'ep_profissionaisposts_metaboxes' );
 
function ept_profissionais_infos() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_profissionaisposts_nonce' );
    // The metabox HTML
    $profissionais_cargo = get_post_meta( $post->ID, '_profissionais_cargo', true );
	echo '<p>Cargo:</p>';
    echo '<input type="text" name="_profissionais_cargo" value="' . $profissionais_cargo  . '"  style="width:99%"/>';
    $profissionais_area = get_post_meta( $post->ID, '_profissionais_area', true );
	echo '<p>Área de Atuação:</p>';
    echo '<input type="text" name="_profissionais_area" value="' . $profissionais_area  . '"  style="width:99%"/>';
}

function ept_profissionais_contatos() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_profissionaisposts_nonce' );
    // The metabox HTML
    $profissionais_email = get_post_meta( $post->ID, '_profissionais_email', true );
	echo '<p>E-mail:</p>';
    echo '<input type="e-mail" name="_profissionais_email" value="' . $profissionais_email  . '"  style="width:99%"/>';
    $profissionais_telefone = get_post_meta( $post->ID, '_profissionais_telefone', true );
	echo '<p>Telefone:</p>';
    echo '<input type="text" name="_profissionais_telefone" value="' . $profissionais_telefone  . '"  style="width:99%"/>';
    $profissionais_vcard = get_post_meta( $post->ID, '_profissionais_vcard', true );
	echo '<p>Link vCard:</p>';
    echo '<input type="text" name="_profissionais_vcard" value="' . $profissionais_vcard  . '"  style="width:99%"/>';
}

function ept_profissionais_linguas() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_profissionaisposts_nonce' );
    // The metabox HTML
	echo '<p>* Não é necessário preencher todos os campos</p>';
    $profissionais_linguas1 = get_post_meta( $post->ID, '_profissionais_linguas1', true );
	echo '<p>Línguas 1:</p>';
    echo '<input type="text" name="_profissionais_linguas1" value="' . $profissionais_linguas1  . '"  style="width:99%"/>';
    $profissionais_linguas2 = get_post_meta( $post->ID, '_profissionais_linguas2', true );
	echo '<p>Línguas 2:</p>';
    echo '<input type="text" name="_profissionais_linguas2" value="' . $profissionais_linguas2  . '"  style="width:99%"/>';
    $profissionais_linguas3 = get_post_meta( $post->ID, '_profissionais_linguas3', true );
	echo '<p>Línguas 3:</p>';
    echo '<input type="text" name="_profissionais_linguas3" value="' . $profissionais_linguas3  . '"  style="width:99%"/>';
    $profissionais_linguas4 = get_post_meta( $post->ID, '_profissionais_linguas4', true );
	echo '<p>Línguas 4:</p>';
    echo '<input type="text" name="_profissionais_linguas4" value="' . $profissionais_linguas4  . '"  style="width:99%"/>';
}

function ept_profissionais_background() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_profissionaisposts_nonce' );
    // The metabox HTML
	echo '<p>* Não é necessário preencher todos os campos</p>';
    $profissionais_background1 = get_post_meta( $post->ID, '_profissionais_background1', true );
    echo '<input type="text" name="_profissionais_background1" value="' . $profissionais_background1  . '"  style="width:99%"/><br>';
    $profissionais_background2 = get_post_meta( $post->ID, '_profissionais_background2', true );
    echo '<input type="text" name="_profissionais_background2" value="' . $profissionais_background2  . '"  style="width:99%"/><br>';
    $profissionais_background3 = get_post_meta( $post->ID, '_profissionais_background3', true );
    echo '<input type="text" name="_profissionais_background3" value="' . $profissionais_background3  . '"  style="width:99%"/><br>';
    $profissionais_background4 = get_post_meta( $post->ID, '_profissionais_background4', true );
    echo '<input type="text" name="_profissionais_background4" value="' . $profissionais_background4  . '"  style="width:99%"/><br>';
    $profissionais_background5 = get_post_meta( $post->ID, '_profissionais_background5', true );
    echo '<input type="text" name="_profissionais_background5" value="' . $profissionais_background5  . '"  style="width:99%"/>';
}


// Save the Metabox Data
function ep_profissionaisposts_save_meta( $post_id, $post ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( !isset( $_POST['ep_profissionaisposts_nonce'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['ep_profissionaisposts_nonce'], plugin_basename( __FILE__ ) ) )
        return;
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ) )
        return;
 
    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though
 	$profissionais_meta_save['_profissionais_cargo'] = $_POST['_profissionais_cargo'];
 	$profissionais_meta_save['_profissionais_area'] = $_POST['_profissionais_area'];
 	$profissionais_meta_save['_profissionais_email'] = $_POST['_profissionais_email'];
 	$profissionais_meta_save['_profissionais_telefone'] = $_POST['_profissionais_telefone'];
 	$profissionais_meta_save['_profissionais_vcard'] = $_POST['_profissionais_vcard'];
 	$profissionais_meta_save['_profissionais_linguas1'] = $_POST['_profissionais_linguas1'];
 	$profissionais_meta_save['_profissionais_linguas2'] = $_POST['_profissionais_linguas2'];
 	$profissionais_meta_save['_profissionais_linguas3'] = $_POST['_profissionais_linguas3'];
 	$profissionais_meta_save['_profissionais_linguas4'] = $_POST['_profissionais_linguas4'];
 	$profissionais_meta_save['_profissionais_background1'] = $_POST['_profissionais_background1'];
 	$profissionais_meta_save['_profissionais_background2'] = $_POST['_profissionais_background2'];
 	$profissionais_meta_save['_profissionais_background3'] = $_POST['_profissionais_background3'];
 	$profissionais_meta_save['_profissionais_background4'] = $_POST['_profissionais_background4'];
 	$profissionais_meta_save['_profissionais_background5'] = $_POST['_profissionais_background5'];
 	
    // Add values of $events_meta as custom fields
    foreach ( $profissionais_meta_save as $key => $value ) { // Cycle through the $events_meta array!
        if ( $post->post_type == 'revision' ) return; // Don't store custom data twice
        $value = implode( ',', (array)$value ); // If $value is an array, make it a CSV (unlikely)
        if ( get_post_meta( $post->ID, $key, false ) ) { // If the custom field already has a value
            update_post_meta( $post->ID, $key, $value );
        } else { // If the custom field doesn't have a value
            add_post_meta( $post->ID, $key, $value );
        }
        if ( !$value ) delete_post_meta( $post->ID, $key ); // Delete if blank
    }
}
add_action( 'save_post', 'ep_profissionaisposts_save_meta', 1, 2 );
