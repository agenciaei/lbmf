<?php


remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop' , 99);
add_filter( 'the_content', 'shortcode_unautop',100 );


//////////////////////////////////////////////////////////////////
// Coluna shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('colunas', 'shortcode_colunas');
	function shortcode_colunas($atts, $content = null) {
		extract(shortcode_atts(array(
        'qtde'      => '#',
    ), $atts));
	$out .= '<div class="col-md-' . $qtde . '';
	$out .= '">';
	$out .= do_shortcode($content);
	$out .= '</div>';
	
   return $out;

	}
	
//////////////////////////////////////////////////////////////////
// Linha shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('row', 'shortcode_row');
	function shortcode_row($atts, $content = null) {
	$out .= '<div class="row">';
	$out .= do_shortcode($content);
	$out .= '</div>';
	
   return $out;

	}	

//////////////////////////////////////////////////////////////////
// Coluna Interna shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('colunasI', 'shortcode_colunasI');
	function shortcode_colunasI($atts, $content = null) {
		extract(shortcode_atts(array(
        'qtde'      => '#',
    ), $atts));
	$out .= '<div class="col-md-' . $qtde . '';
	$out .= '">';
	$out .= do_shortcode($content);
	$out .= '</div>';
	
   return $out;

	}
	
//////////////////////////////////////////////////////////////////
// Linha Interna shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('rowI', 'shortcode_rowI');
	function shortcode_rowI($atts, $content = null) {
	$out .= '<div class="row">';
	$out .= do_shortcode($content);
	$out .= '</div>';
	
   return $out;

	}	

//////////////////////////////////////////////////////////////////
// Container shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('container', 'shortcode_container');
	function shortcode_container($atts, $content = null) {
	$out .= '<section class="container">';
	$out .= do_shortcode($content);
	$out .= '</section>';
	
   return $out;

	}	

//////////////////////////////////////////////////////////////////
// Table Responsive
//////////////////////////////////////////////////////////////////
add_shortcode('table', 'shortcode_table');
	function shortcode_table($atts, $content = null) {
	$out .= '<div class="table-responsive">';
	$out .= '<table class="table">';
	$out .= do_shortcode($content);
	$out .= '</table>';
	$out .= '</div>';
	
   return $out;

	}	


//////////////////////////////////////////////////////////////////
// Thead Table
//////////////////////////////////////////////////////////////////
add_shortcode('thead', 'shortcode_thead');
	function shortcode_thead($atts, $content = null) {
	$out .= '<thead>';
	$out .= '<tr>';
	$out .= do_shortcode($content);
	$out .= '</tr>';
	$out .= '</thead>';
	
   return $out;

	}		

//////////////////////////////////////////////////////////////////
// TH Table
//////////////////////////////////////////////////////////////////
add_shortcode('th', 'shortcode_th');
	function shortcode_th($atts, $content = null) {
		extract(shortcode_atts(array(
        'colunas'      => ' ',
        'linhas'      => ' ',
    ), $atts));
	$out .= '<th';
	if($colunas !=' '){
	$out .= ' colspan="'.$colunas.'"';
	}
	if($linhas !=' '){
	$out .= ' rowspan="'.$linhas.'"';
	}
	$out .= '>';
	$out .= do_shortcode($content);
	$out .= '</th>';
	
   return $out;

	}		

//////////////////////////////////////////////////////////////////
// TBody Table
//////////////////////////////////////////////////////////////////
add_shortcode('tbody', 'shortcode_tbody');
	function shortcode_tbody($atts, $content = null) {
	$out .= '<tbody>';
	$out .= do_shortcode($content);
	$out .= '</tbody>';
	
   return $out;

	}	

//////////////////////////////////////////////////////////////////
// TR Table
//////////////////////////////////////////////////////////////////
add_shortcode('tr', 'shortcode_tr');
	function shortcode_tr($atts, $content = null) {
	$out .= '<tr>';
	$out .= do_shortcode($content);
	$out .= '</tr>';
	
   return $out;

	}	

//////////////////////////////////////////////////////////////////
// TD Tabke
//////////////////////////////////////////////////////////////////
add_shortcode('celula', 'shortcode_celula');
	function shortcode_celula($atts, $content = null) {
		extract(shortcode_atts(array(
        'colunas'      => ' ',
        'linhas'      => ' ',
    ), $atts));
	$out .= '<td';
	if($colunas !=' '){
	$out .= ' colspan="'.$colunas.'"';
	}
	if($linhas !=' '){
	$out .= ' rowspan="'.$linhas.'"';
	}
	$out .= '>';
	$out .= do_shortcode($content);
	$out .= '</td>';
	
   return $out;

	}

//////////////////////////////////////////////////////////////////
// Botões
//////////////////////////////////////////////////////////////////
add_shortcode('btn', 'shortcode_btn');
	function shortcode_btn($atts, $content = null) {
		extract(shortcode_atts(array(
        'class'      => '#',
        'title'      => '#',
        'link'      => 'http://',
    ), $atts));
	$out .= '<a class="btn ' . $class . '';
	$out .= '" title="' .$title.'" href=" '.$link.'" role="button">';
	$out .= do_shortcode($content);
	$out .='<span class="glyphicon glyphicon-chevron-right"></span>';
	$out .= '</a>';
	
   return $out;

	}
	
//////////////////////////////////////////////////////////////////
// Botões Links Externos
//////////////////////////////////////////////////////////////////
add_shortcode('btn-e', 'shortcode_btn_e');
	function shortcode_btn_e($atts, $content = null) {
		extract(shortcode_atts(array(
        'class'      => '#',
        'title'      => '#',
        'link'      => 'http://',
    ), $atts));
	$out .= '<a class="btn ' . $class . '';
	$out .= '" title="' .$title.'" href=" '.$link.'" role="button" target="_blank">';
	$out .= do_shortcode($content);
	$out .='<span class="glyphicon glyphicon-chevron-right"></span>';
	$out .= '</a>';
	
   return $out;

	}


//////////////////////////////////////////////////////////////////
// List Unstyled
//////////////////////////////////////////////////////////////////
add_shortcode('unstyled', 'shortcode_unstyled');
function shortcode_unstyled( $atts, $content = null ) {
	$content = str_replace('<ul>', '<ul class="list-unstyled">', do_shortcode($content));
	return $content;	
}

//////////////////////////////////////////////////////////////////
// List Romana
//////////////////////////////////////////////////////////////////
add_shortcode('lista-romana', 'shortcode_romana');
function shortcode_romana( $atts, $content = null ) {
	$content = str_replace('<ol>', '<ol class="lista-romana">', do_shortcode($content));
	return $content;	
}

//////////////////////////////////////////////////////////////////
// List Alfabética
//////////////////////////////////////////////////////////////////
add_shortcode('lista-alfabetica', 'shortcode_alfabetica');
function shortcode_alfabetica( $atts, $content = null ) {
	$content = str_replace('<ol>', '<ol class="lista-alfabetica">', do_shortcode($content));
	return $content;	
}


?>