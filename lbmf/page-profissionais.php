<?php get_header(); 

/* Template Name: Profissionais */

?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
          <section class="hidden-xs hidden-sm feature bg-<?php $imagem = get_post_meta( $post->ID, 'imagem', true ); echo $imagem; ?>" data-stellar-background-ratio="0.5"></section>
          <section class="hidden-xs visible-sm feature bg-<?php $imagem = get_post_meta( $post->ID, 'imagem', true ); echo $imagem; ?>"></section>
          <section class="visible-xs feature-mobile bg-<?php $imagem = get_post_meta( $post->ID, 'imagem', true ); echo $imagem; ?>-mobile"></section>
      <section class="sub-header">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-3 col-xs-12">
            <div class="retangulo-page-header"></div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="page-header">
                <h1 class="titulo-pagina">
                    <?php the_title(); ?><br>
                </h1>
                    <small><?php $texto = get_post_meta( $post->ID, 'texto', true ); echo $texto; ?></small>
                <div class="barra-page-header"></div>
            </div>
            </div>
            <div class="col-md-4 col-sm-3 col-xs-12">
                <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
            </div>
            </div>
        </div>
      </section>
<?php $imagem =  wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' ); ?>
<?php endwhile; endif; ?>
      <section class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
            <?php /*wp_nav_menu(array(
            'container'       => false,
            'items_wrap'      => '<ul id="%1$s" class="letras">%3$s</ul>',
            'theme_location'    => 'profissionais',
            'walker'          => new twitter_bootstrap_nav_walker
            ));*/
            ?>
                </div>
                <div class="col-md-6 col-sm-8">
                    <h2 class="titulo-cargo"><?php _e('Sócios', 'lbmf'); ?></h2>
                        <div class="barra-area-profissional"></div>
                   <?php $newsArgs = array( 
                   'post_type' => 'profissionais',
                   'order' => 'ASC',
                   'meta_query'=> array(
                       array(
                            'key'     => '_profissionais_cargo',
                            'value'   => 'Sócios',
                            'compare' => '='
                       )
                   ),
                   );  
                   
      $newsLoop = new WP_Query( $newsArgs );                  
      $i = 0;

      while ( $newsLoop->have_posts() ) : $newsLoop->the_post();
        $i++;
                   
$cargo = get_post_meta( $post->ID, '_profissionais_cargo', true );
$area = get_post_meta( $post->ID, '_profissionais_area', true );
$email = get_post_meta( $post->ID, '_profissionais_email', true );
$telefone = get_post_meta( $post->ID, '_profissionais_telefone', true );
$vcard = get_post_meta( $post->ID, '_profissionais_vcard', true );
$linguas1 = get_post_meta( $post->ID, '_profissionais_linguas1', true );
$linguas2 = get_post_meta( $post->ID, '_profissionais_linguas2', true );
$linguas3 = get_post_meta( $post->ID, '_profissionais_linguas3', true );
$linguas4 = get_post_meta( $post->ID, '_profissionais_linguas4', true );
$background1 = get_post_meta( $post->ID, '_profissionais_background1', true );
$background2 = get_post_meta( $post->ID, '_profissionais_background2', true );
$background3 = get_post_meta( $post->ID, '_profissionais_background3', true );
$background4 = get_post_meta( $post->ID, '_profissionais_background4', true );

                   
                   ?>

                    <div class="row box-profissionais">
                        <div class="col-md-8 col-sm-8">
                        <h2 class="titulo-profissional"><a data-toggle="collapse" href="#socios<?php echo $i; ?>" aria-expanded="false" aria-controls="socios<?php echo $i; ?>"><?php the_title(); ?></a></h2>
                        <div class="collapse" id="socios<?php echo $i; ?>">
                            <p><?php echo $area; ?></p>
                            <p class="info-profissional"><a href="mailto:<?php echo $email; ?>" class="email-prof"><?php echo $email; ?></a><br>
                            +55 11 4118-1000 / <?php echo $telefone; ?><br>
                            <a href="http://www.lbmf.com.br/vCards/<?php echo $vcard; ?>" class="btn-vCard"><?php _e('Download do vCard', 'lbmf'); ?></a></p>
                            <p><a href="<?php the_permalink(); ?>" class="btn-lateral"><?php _e('PERFIL COMPLETO', 'lbmf'); ?></a></p>
                        </div>
                        </div>
                    </div>
                    <?php 
                    
                    wp_reset_postdata();
                    
                    endwhile; ?>
                    <h2 class="titulo-cargo"><?php _e('Associados', 'lbmf'); ?></h2>
                        <div class="barra-cargo"></div>
                   <?php $newsArgs = array( 
                   'post_type' => 'profissionais',
                   'order' => 'ASC',
                   'posts_per_page' => -1,
                   'meta_query'=> array(
                       array(
                            'key'     => '_profissionais_cargo',
                            'value'   => 'Associados',
                            'compare' => '='
                       )
                   ),
                   );  
                   
      $newsLoop = new WP_Query( $newsArgs );                  
      $a = 0;

      while ( $newsLoop->have_posts() ) : $newsLoop->the_post();
        $a++;
                   
$cargo = get_post_meta( $post->ID, '_profissionais_cargo', true );
$area = get_post_meta( $post->ID, '_profissionais_area', true );
$email = get_post_meta( $post->ID, '_profissionais_email', true );
$telefone = get_post_meta( $post->ID, '_profissionais_telefone', true );
$vcard = get_post_meta( $post->ID, '_profissionais_vcard', true );
$linguas1 = get_post_meta( $post->ID, '_profissionais_linguas1', true );
$linguas2 = get_post_meta( $post->ID, '_profissionais_linguas2', true );
$linguas3 = get_post_meta( $post->ID, '_profissionais_linguas3', true );
$linguas4 = get_post_meta( $post->ID, '_profissionais_linguas4', true );
$background1 = get_post_meta( $post->ID, '_profissionais_background1', true );
$background2 = get_post_meta( $post->ID, '_profissionais_background2', true );
$background3 = get_post_meta( $post->ID, '_profissionais_background3', true );
$background4 = get_post_meta( $post->ID, '_profissionais_background4', true );

                   
                   ?>

                    <div class="row box-profissionais">
                        <div class="col-md-8 col-sm-8">
                        <h2 class="titulo-profissional"><a data-toggle="collapse" href="#associados<?php echo $a; ?>" aria-expanded="false" aria-controls="associados<?php echo $a; ?>"><?php the_title(); ?></a></h2>
                        <div class="collapse" id="associados<?php echo $a; ?>">
                            <p><?php echo $area; ?></p>
                            <p class="info-profissional"><a href="mailto:<?php echo $email; ?>" class="email-prof"><?php echo $email; ?></a><br>
                            +55 11 4118-1000 / <?php echo $telefone; ?><br>
                            <a href="http://www.lbmf.com.br/vCards/<?php echo $vcard; ?>" class="btn-vCard"><?php _e('Download do vCard', 'lbmf'); ?></a></p>
                            <p><a href="<?php the_permalink(); ?>" class="btn-lateral"><?php _e('PERFIL COMPLETO', 'lbmf'); ?></a></p>
                        </div>
                        </div>
                    </div>
                    <?php 
                    
                    wp_reset_postdata();
                    
                    endwhile; ?>
                   <?php $newsArgs = array( 
                   'post_type' => 'profissionais',
                   'order' => 'ASC',
                   'posts_per_page' => -1,
                   'meta_query'=> array(
                       array(
                            'key'     => '_profissionais_cargo',
                            'value'   => 'Consultores',
                            'compare' => '='
                       )
                   ),
                   );  
        $cont = 0;         
      $newsLoop = new WP_Query( $newsArgs );                  
                        
      while ( $newsLoop->have_posts() ) : $newsLoop->the_post(); $cont++;
 
                   
$cargo = get_post_meta( $post->ID, '_profissionais_cargo', true );
$area = get_post_meta( $post->ID, '_profissionais_area', true );
$email = get_post_meta( $post->ID, '_profissionais_email', true );
$telefone = get_post_meta( $post->ID, '_profissionais_telefone', true );
$vcard = get_post_meta( $post->ID, '_profissionais_vcard', true );
$linguas1 = get_post_meta( $post->ID, '_profissionais_linguas1', true );
$linguas2 = get_post_meta( $post->ID, '_profissionais_linguas2', true );
$linguas3 = get_post_meta( $post->ID, '_profissionais_linguas3', true );
$linguas4 = get_post_meta( $post->ID, '_profissionais_linguas4', true );
$background1 = get_post_meta( $post->ID, '_profissionais_background1', true );
$background2 = get_post_meta( $post->ID, '_profissionais_background2', true );
$background3 = get_post_meta( $post->ID, '_profissionais_background3', true );
$background4 = get_post_meta( $post->ID, '_profissionais_background4', true );

                   
                   ?>
                <?php if($cont == 1){ ?>
                <h2 class="titulo-cargo"><?php _e('Consultores', 'lbmf'); ?></h2>
                        <div class="barra-cargo"></div>
                <?php } ?>
                    <div class="row box-profissionais">
                        <div class="col-md-8 col-sm-8">
                        <h2 class="titulo-profissional"><a data-toggle="collapse" href="#consultores<?php echo $cont; ?>" aria-expanded="false" aria-controls="consultores<?php echo $cont; ?>"><?php the_title(); ?></a></h2>
                        <div class="collapse" id="consultores<?php echo $cont; ?>">
                            <p><?php echo $area; ?></p>
                            <p class="info-profissional"><a href="mailto:<?php echo $email; ?>" class="email-prof"><?php echo $email; ?></a><br>
                            +55 11 4118-1000 / <?php echo $telefone; ?><br>
                            <a href="http://www.lbmf.com.br/vCards/<?php echo $vcard; ?>" class="btn-vCard"><?php _e('Download do vCard', 'lbmf'); ?></a></p>
                            <p><a href="<?php the_permalink(); ?>" class="btn-lateral"><?php _e('PERFIL COMPLETO', 'lbmf'); ?></a></p>
                        </div>
                        </div>
                    </div>
                    <?php 
                    
                    wp_reset_postdata();
                    
                    endwhile; ?>
                   <?php $newsArgs = array( 
                   'post_type' => 'profissionais',
                   'order' => 'ASC',
                   'meta_query'=> array(
                       array(
                            'key'     => '_profissionais_cargo',
                            'value'   => 'Estagiários',
                            'compare' => '='
                       )
                   ),
                   );  
        $cont = 0;         
      $newsLoop = new WP_Query( $newsArgs );                  
                        
      while ( $newsLoop->have_posts() ) : $newsLoop->the_post(); $cont++;
 
                   
$cargo = get_post_meta( $post->ID, '_profissionais_cargo', true );
$area = get_post_meta( $post->ID, '_profissionais_area', true );
$email = get_post_meta( $post->ID, '_profissionais_email', true );
$telefone = get_post_meta( $post->ID, '_profissionais_telefone', true );
$vcard = get_post_meta( $post->ID, '_profissionais_vcard', true );
$linguas1 = get_post_meta( $post->ID, '_profissionais_linguas1', true );
$linguas2 = get_post_meta( $post->ID, '_profissionais_linguas2', true );
$linguas3 = get_post_meta( $post->ID, '_profissionais_linguas3', true );
$linguas4 = get_post_meta( $post->ID, '_profissionais_linguas4', true );
$background1 = get_post_meta( $post->ID, '_profissionais_background1', true );
$background2 = get_post_meta( $post->ID, '_profissionais_background2', true );
$background3 = get_post_meta( $post->ID, '_profissionais_background3', true );
$background4 = get_post_meta( $post->ID, '_profissionais_background4', true );

                   
                   ?>
                <?php if($cont == 1){ ?>
                    <h2 class="titulo-cargo"><?php _e('Estagiários', 'lbmf'); ?></h2>
                        <div class="barra-cargo"></div>
                <?php } ?>

                    <div class="row box-profissionais">
                        <div class="col-md-8 col-sm-8">
                        <h2 class="titulo-profissional"><a data-toggle="collapse" href="#estagiarios<?php echo $cont; ?>" aria-expanded="false" aria-controls="estagiarios<?php echo $cont; ?>"><?php the_title(); ?></a></h2>
                        <div class="collapse" id="estagiarios<?php echo $cont; ?>">
                            <p><?php echo $area; ?></p>
                            <p class="info-profissional"><a href="mailto:<?php echo $email; ?>" class="email-prof"><?php echo $email; ?></a><br>
                            +55 11 4118-1000 / <?php echo $telefone; ?><br>
                            <a href="http://www.lbmf.com.br/vCards/<?php echo $vcard; ?>" class="btn-vCard"><?php _e('Download do vCard', 'lbmf'); ?></a></p>
                            <p><a href="<?php the_permalink(); ?>" class="btn-lateral"><?php _e('PERFIL COMPLETO', 'lbmf'); ?></a></p>
                        </div>
                        </div>
                    </div>
                    <?php 
                    
                    wp_reset_postdata();
                    
                    endwhile; ?>

                </div>
                <div class="col-md-3 col-md-offset-1 col-sm-4">
                <h2 class="titulo-home text-right"><?php _e('DESENVOLVIMENTO<br>PROFISSIONAL', 'lbmf'); ?></h2>
                    <div class="barra-titulo-oportunidade"></div>
                    <p class="text-right texto-oportunidade"><?php _e('Entenda como os profissionais do LBMF constantemente crescem e ampliam sua experiência.', 'lbmf'); ?></p>
                    <p class="text-right"><a href="<?php echo home_url(); ?>/<?php _e('desenvolvimento-profissional', 'lbmf'); ?>" class="btn-lateral"><?php _e('CONHEÇA', 'lbmf'); ?></a></p>
                <h2 class="titulo-home text-right"><?php _e('OPORTUNIDADES<br>DE CARREIRA', 'lbmf'); ?></h2>
                    <div class="barra-titulo-oportunidade"></div>
                    <p class="text-right texto-oportunidade"><?php _e('Saiba como ser parte da equipe LBMF.', 'lbmf'); ?></p>
                    <p class="text-right"><a href="<?php echo home_url(); ?>/<?php _e('oportunidades-de-carreira', 'lbmf'); ?>" class="btn-lateral"><?php _e('FAÇA PARTE', 'lbmf'); ?></a></p>
                    <img src="<?php echo $imagem[0]; ?>" alt="Profissionais" class="img-responsive">
                </div>
            </div>
        </div>
      </section>
  <?php get_footer(); ?>
  
