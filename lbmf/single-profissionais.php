<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); 

$cargo = get_post_meta( $post->ID, '_profissionais_cargo', true );
$area = get_post_meta( $post->ID, '_profissionais_area', true );
$email = get_post_meta( $post->ID, '_profissionais_email', true );
$telefone = get_post_meta( $post->ID, '_profissionais_telefone', true );
$vcard = get_post_meta( $post->ID, '_profissionais_vcard', true );
$linguas1 = get_post_meta( $post->ID, '_profissionais_linguas1', true );
$linguas2 = get_post_meta( $post->ID, '_profissionais_linguas2', true );
$linguas3 = get_post_meta( $post->ID, '_profissionais_linguas3', true );
$linguas4 = get_post_meta( $post->ID, '_profissionais_linguas4', true );
$background1 = get_post_meta( $post->ID, '_profissionais_background1', true );
$background2 = get_post_meta( $post->ID, '_profissionais_background2', true );
$background3 = get_post_meta( $post->ID, '_profissionais_background3', true );
$background4 = get_post_meta( $post->ID, '_profissionais_background4', true );

?>

    	  <section class="hidden-xs hidden-sm feature bg-reuniao" data-stellar-background-ratio="0.5"></section>
    	  <section class="visible-sm feature bg-reuniao"></section>
    	  <section class="visible-xs feature-mobile bg-reuniao-mobile"></section>
   	  <section class="sub-header">
      	<div class="container">
        	<div class="row">
            	<div class="col-md-2 col-sm-3 col-xs-12">
        	<div class="retangulo-page-header"></div>
            	</div>
                <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="page-header">
            	<h1 class="titulo-pagina">
                	<?php the_title(); ?><br>
                </h1>
                    <small><?php echo $area; ?></small>
                <div class="barra-page-header"></div>
            </div>
            </div>
            <div class="col-md-4 col-sm-3 col-xs-12">
            	<ol class="breadcrumb">
                    <li><a href="http://www.lbmf.com.br">Home</a></li>
                    <li><a href="http://www.lbmf.com.br/<?php _e('profissionais','lbmf'); ?>"><?php _e('Profissionais','lbmf'); ?></a></li>
                    <li class="active"><?php the_title(); ?></li></ol>
            </div>
            </div>
        </div>
      </section>
      <section class="wrapper">
      	<div class="container">
			<div class="row">
            	<div class="col-md-3 col-sm-3">
                	<?php $imagem =  wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' ); ?>
                    <img src="<?php echo $imagem[0]; ?>" alt="" class="img-responsive" class="<?php the_title(); ?>">
                    <div class="box-info-profissionais">
                     <p class="info-profissional"><a href="mailto:<?php echo $email; ?>" class="email-prof"><?php echo $email; ?></a><br>
                     +55 11 4118-1000<br><?php echo $telefone; ?><br>
                     <a href="http://www.lbmf.com.br/vCards/<?php echo $vcard; ?>" class="btn-vCard"><?php _e('Download do vCard', 'lbmf'); ?></a></p>
                     <div class="hidden-xs">
                     <h2 class="titulo-interna"><?php _e('Idiomas','lbmf'); ?></h2>
                     <div class="barra-titulo-interna"></div>
                     <ul>
                     	<li><?php echo $linguas1; ?></li>
                        <?php if(empty($linguas2)){ ?>
                        <?php } else{?>
                        <li><?php echo $linguas2; ?></li>
                        <?php } ?>
                        <?php if(empty($linguas3)){ ?>
                        <?php } else{?>
                        <li><?php echo $linguas3; ?></li>
                        <?php } ?>
                        <?php if(empty($linguas4)){ ?>
                        <?php } else{?>
                        <li><?php echo $linguas4; ?></li>
                        <?php } ?>
                     </ul>
                     </div>
                     </div>
                </div>
            	<div class="col-md-5 col-sm-6">
                
               	  <?php the_content('Read the rest of this entry &raquo;'); ?>
                  
                  <h2 class="titulo-interna">Background</h2>
                    <div class="barra-titulo-interna"></div>
                     <ul>
                     	<li><?php echo $background1; ?></li>
                        <?php if(empty($background2)){ ?>
                        <?php } else{?>
                        <li><?php echo $background2; ?></li>
                        <?php } ?>
                        <?php if(empty($background3)){ ?>
                        <?php } else{?>
                        <li><?php echo $background3; ?></li>
                        <?php } ?>
                        <?php if(empty($background4)){ ?>
                        <?php } else{?>
                        <li><?php echo $background4; ?></li>
                        <?php } ?>
                     </ul>
                   <div class="visible-xs">
                     <h2 class="titulo-interna"><?php _e('Línguas','lbmf'); ?></h2>
                     <div class="barra-titulo-interna"></div>
                     <ul>
                     	<li><?php echo $linguas1; ?></li>
                        <?php if(empty($linguas2)){ ?>
                        <?php } else{?>
                        <li><?php echo $linguas2; ?></li>
                        <?php } ?>
                        <?php if(empty($linguas3)){ ?>
                        <?php } else{?>
                        <li><?php echo $linguas3; ?></li>
                        <?php } ?>
                        <?php if(empty($linguas4)){ ?>
                        <?php } else{?>
                        <li><?php echo $linguas4; ?></li>
                        <?php } ?>
                     </ul>
                     </div>


                </div>
            	<div class="col-md-3 col-md-offset-1 col-sm-3">
                    <img src="<?php bloginfo('template_directory'); ?>/img/cadeira.jpg" alt="" class="img-responsive">
                </div>
            </div>
        </div>
      </section>
<?php endwhile; endif; ?>
  <?php get_footer(); ?>
  
